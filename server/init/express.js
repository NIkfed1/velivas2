import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import  cors  from 'cors';
import proxy from 'express-http-proxy';

const session = require('express-session');

const flash = require('req-flash');

export default (app) => {

    app.use(cors());
    app.set('port', (process.env.PORT || 3000));
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));


}


