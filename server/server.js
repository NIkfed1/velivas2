import express  from 'express';
import InitExpress from './init/express';
import InitRoutes from './init/routes';
import renderMiddleware from './render/middleware'


const app = express();

InitExpress(app);

InitRoutes(app);

app.get('*', renderMiddleware);

app.listen(app.get('port'), () => {
    console.log(`Server listening on: ${app.get('port')}`);
});
