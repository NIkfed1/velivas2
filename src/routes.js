import React from 'react';
import { IndexRoute, Route} from 'react-router';

import App from './App';
import Prices from './Prices';
import Footer from './Footer';
import Index from './Body'
import Contacts from './Contacts';
import About from './About';
import Portfolio from './Portfolio'


export default (store) => {

    return (
        <Route path="/" component={App}>
            <IndexRoute component={Index} />

                    <Route path="/index" component={Index}/>
                    <Route path="/prices" component={Prices}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route path="/portfolio" component={Portfolio}/>
                    <Route path="/about" component={About}/>

        </Route>
    );
};
