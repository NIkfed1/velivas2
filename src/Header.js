import autobind from 'react-autobind';
import React, { Component } from 'react';
import {NavDropdown,NavItem,Navbar,Nav, MenuItem,Link} from 'react-bootstrap';

import './css/bootstrap.css';
import './css/index.css';

class Header extends Component {

    constructor(props) {
        super(props);
        autobind(this);
    }

    render() {
        return (
            <div>
                <Navbar>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="#">Maria Velivas</a>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                        <NavItem eventKey={1} href="/index">Главная</NavItem>
                        <NavItem eventKey={2} href="/about">Обо мне</NavItem>
                        <NavItem eventKey={3} href="/portfolio">Портфолио</NavItem>
                        <NavItem eventKey={4} href="/prices">Услуги и Цены</NavItem>
                        <NavItem eventKey={5} href="/contacts">Контакты</NavItem>
                    </Nav>
                </Navbar>


            </div>
        );
    }
}

export default Header;