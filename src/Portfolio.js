import autobind from 'react-autobind';
import React, { Component } from 'react';
import { Button,Jumbotron} from 'react-bootstrap';
import {Grid, Row, Col, Image, Table,PanelGroup,Panel, PageHeader} from 'react-bootstrap'
import Instafeed from 'react-instafeed';


import './css/bootstrap.css';
import './css/index.css';

class Prices extends Component {

    constructor(props) {
        super(props);
        autobind(this);

    }



    render() {

        const instafeedTarget = 'instafeed';
        const template =   `<img src='{{image}}' responsive />`

        return (
            <div className="portfolio">

                <PageHeader>Портфолио<small></small></PageHeader>

                <Jumbotron>
                    <h2>Последние проекты</h2>
                    <p></p>
                    <p></p>
                    <div id={instafeedTarget}>
                        <Instafeed
                            limit='60'
                            ref='instafeed'
                            resolution='low_resolution'
                            sortBy='most-recent'
                            target={instafeedTarget}
                            template={template}
                            userId='self'
                            clientId='b8a3b804f63a419fa76101be2957017b'
                            accessToken='1560504849.b8a3b80.8f896a0609c842a99fe49b2ea305d396'
                        />
                    </div>
                </Jumbotron>

            </div>
        );
    }


}


export default Prices;