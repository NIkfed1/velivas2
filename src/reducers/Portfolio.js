import { combineReducers } from 'redux';
import * as types from '../types';

const defaultState = false;

const lastPhotos = (state = defaultState, action) => {
    switch(action.type) {
        case types.PORTFOLIO_LAST_PHOTOS:
            return action;

        default:
            return state;
    }
}

const portfolio = combineReducers({
    lastPhotos
});

export default portfolio;
