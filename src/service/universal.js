//import { apiEndpoint } from '../../config/app';
import createRestApiClient from '../utils/createRestAPIClient';


export default () => {
    const client = createRestApiClient().withConfig({ baseURL: 'http://localhost:3001' });
    return {
        getLastPhotos: () => client.request({
            method: 'GET',
            url: '/v2/get'
        })
    };
};