import autobind from 'react-autobind';
import React, { Component } from 'react';
import { Button,Jumbotron} from 'react-bootstrap';
import {Grid, Row, Col, Image, Table,PanelGroup,Panel, PageHeader} from 'react-bootstrap'


import './css/bootstrap.css';
import './css/index.css';

class Prices extends Component {

    constructor(props) {
        super(props);
        autobind(this);

    }



    render() {


        return (
            <div className="prices">

                <PageHeader>Услуги & Цены<small></small></PageHeader>

                <Jumbotron>
                    <PanelGroup defaultActiveKey="1" accordion>
                        <Panel header="Комплекс Макияж + прическа + репетиция" eventKey="1">До 30 апреля 2017 г. :<br/>
                            пятница и суббота 13 000 руб;<br/>
                            понедельник-четверг, вс. 12 000 руб.<br/><br/>
                            С 1 мая 2017 г. :<br/>
                            пятница и суббота 14 000 руб;<br/>
                            понедельник-четверг, вс. 13 000 руб.</Panel>
                        <Panel header="Комплекс Макияж + прическа (без репетиции)" eventKey="2">
                            До 30 апреля 2017 г. :<br/>
                            пятница и суббота 9 000 руб;<br/>
                            понедельник-четверг, вс. 8 000 руб.<br/><br/>
                            С 1 мая 2017 г. :<br/>
                            пятница и суббота 10 000 руб;<br/>
                            понедельник-четверг, вс. 9 000 руб.
                        </Panel>
                    </PanelGroup>
                </Jumbotron>

            </div>
        );
    }


}


export default Prices;