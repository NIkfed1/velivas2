import autobind from 'react-autobind';
import React, { Component } from 'react';

//import HelloWorldPage from 'components/HelloWorldPage';

import { bindActionCreators } from 'redux'
import * as PortfolioAction from './actions/PortfolioActions';

import { connect } from 'react-redux';

import Header from './Header';
import Body from './Body';
import Footer from './Footer'
import Prices from './Prices'

import './css/bootstrap.css';
import './css/index.css';

@connect(state => ({
    portfolio: state.portfolio,
}))
class App extends Component {
    constructor(props) {
        super(props);
        autobind(this);
        console.log('App', props)
    }


    render() {

        return (
            <div>
                <Header/>
                {this.props.children}
                <Footer/>
            </div>
        );
    }
}

export default App