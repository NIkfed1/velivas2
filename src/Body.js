import autobind from 'react-autobind';
import React, { Component } from 'react';
import { Button,Jumbotron} from 'react-bootstrap';
import Instafeed from 'react-instafeed';
import {Grid, Row, Col, Image, PageHeader,Clearfix} from 'react-bootstrap'


import './css/bootstrap.css';
import './css/index.css';

class Body extends Component {

    constructor(props) {
        super(props);
        autobind(this);
        console.log('Body Props', props)
        //props.getLastPhotos();

        this.state = {
            portfolio: null,
            loaded: false
        }
    }

    componentWillUpdate(nextProps) {
        if(this.props !== nextProps) {
            console.log('Portfolio', nextProps)
            this.setState({
                // portfolio: nextProps.portfolio.lastPhotos.data.data.data[0].link
            })
        }
    }

    render() {

        const instafeedTarget = 'instafeed';
        const template =   `<img src='{{image}}' responsive />`

        const dummySentences = ['Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Donec hendrerit tempor tellus.', 'Donec pretium posuere tellus.', 'Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Nulla posuere.', 'Donec vitae dolor.', 'Nullam tristique diam non turpis.', 'Cras placerat accumsan nulla.', 'Nullam rutrum.', 'Nam vestibulum accumsan nisl.'];

            //<img class='instafeed__item__background' src='{{image}}' />


        return (
            <div className="main">
                <div className="logo-block">
                    <div className="name">
                        <p>Maria Velivas</p>
                        <p>Makeups & Hairs</p>
                    </div>
                </div>

                <Jumbotron>
                    <h2>Последние проекты</h2>
                    <p></p>
                    <p><Button bsStyle="primary" href="/portfolio">Смотреть все!</Button></p>
                    <div id={instafeedTarget}>
                        <Instafeed
                            limit='15'
                            ref='instafeed'
                            resolution='low_resolution'
                            sortBy='most-recent'
                            target={instafeedTarget}
                            template={template}
                            userId='self'
                            clientId='b8a3b804f63a419fa76101be2957017b'
                            accessToken='1560504849.b8a3b80.8f896a0609c842a99fe49b2ea305d396'
                        />
                    </div>
                </Jumbotron>

                <Grid>
                    <PageHeader>Сотрудничество <small>любимые партнеры</small></PageHeader>
                    <Row className="show-grid">
                        <Col sm={6} md={3}><code>&lt;{'Col sm={6} md={3}'} /&gt;</code><br/>{dummySentences.slice(0, 6).join(' ')}</Col>
                        <Col sm={6} md={3}><code>&lt;{'Col sm={6} md={3}'} /&gt;</code><br/>{dummySentences.slice(0, 4).join(' ')}</Col>
                        <Clearfix visibleSmBlock><code>&lt;{'Clearfix visibleSmBlock'} /&gt;</code></Clearfix>
                        <Col sm={6} md={3}><code>&lt;{'Col sm={6} md={3}'} /&gt;</code><br/>{dummySentences.slice(0, 6).join(' ')}</Col>
                        <Col sm={6} md={3}><code>&lt;{'Col sm={6} md={3}'} /&gt;</code><br/>{dummySentences.slice(0, 2).join(' ')}</Col>
                    </Row>
                </Grid>

            </div>
        );
    }


 }


 export default Body;