
import { universalService } from '../service';
import * as types from '../types';

function lastPhotos(data) {
    return {
        type: types.PORTFOLIO_LAST_PHOTOS,
        data: data,
        status: data.status
    }
}

export function getLastPhotos(data) {
    return  (dispatch, getState) => {
        console.log('Last photos request', data);
        return universalService().getLastPhotos(data).then((res) => {
            console.log('Response of duplicate', res.status === 200)
            if (res.status === 200) {
                return dispatch(lastPhotos(res))
            }
        })
            .catch(() => {
                console.log('==getLastPhotos error==');
                return dispatch(lastPhotos({
                    status: 409,
                    data: {
                        message: 'Ошибка!'
                    }
                }))
            });
    }
}